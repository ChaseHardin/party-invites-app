﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PartyInvites.Models; // Added guest response so that I can access GuestResponse.cs

namespace PartyInvites.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ViewResult Index()
        {
            int hour = DateTime.Now.Hour;
            ViewBag.Greeting = hour < 12 ? "Good Morning" : "Good Afternoon";
            return View();
        }

        [HttpGet] // This method should only be used to get the requests
        public ViewResult RsvpForm()
        {
            return View();
        }

        [HttpPost] // This method deals with POST requests. 
        public ViewResult RsvpForm(GuestResponse guestResponse)
        {
            if (ModelState.IsValid)
            {
                // TO DO EMAIL response to the party organizer
                return View("Thanks", guestResponse);
            }

            else
            {
                // there is a validation error
                return View();
            }
        }
    }
}